require("dotenv").config();
const jwt = require("jsonwebtoken");
const LocalStorage = require("node-localstorage").LocalStorage;

exports.createAccessToken = (user) => {
	const data = { 
		id: user._id, 
		googleId: user._googleId,
		email: user.email,
		isAdmin: user.isAdmin,
		isActive: user.isActive
	}
	return jwt.sign(data, process.env.LOCAL_SECRET, {})
}

exports.verify = (req, res, next) => {
	let token = localStorage.getItem('token')
	console.log(token);
	if(typeof token !== "undefined" && token !== null){
		//token = token.slice(10, token.length);
		token = token.substring(1,token.length-1);
		//console.log(token)
		return jwt.verify(token, process.env.LOCAL_SECRET, (err, data) => {
			if(err) res.redirect("/login");
			else{
				next();
			}
		})
	}else res.redirect("/login");;
}


exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.substring(1,token.length-1);
		return jwt.verify(token, process.env.LOCAL_SECRET, (err, data) => {
			if(err) return null
			else{
				return jwt.decode(token, {complete: true}).payload
			} 
		})
	}else return null
}


