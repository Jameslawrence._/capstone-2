exports.checkMatchPassword = (password, confirmPassword) => {
	if(password === confirmPassword) return true;
	else return false;
}

exports.checkPasswordLength = (password) => {
	let passwordLength = password.length;
	const passwordLengthRequired = 8;
	if(passwordLength >= passwordLengthRequired) return true
	else return false
}
exports.checkFormatPassword = (password) => {
	const containsSpecialChar = /[#@_.!?]/.test(password); 
	const containsUppercaseLetter = /[A-Z]/.test(password);
	const containsLowercaseLetter = /[a-z]/.test(password);
	const containsNumbers = /[0-9]/.test(password);

	if(containsSpecialChar && containsUppercaseLetter && containsLowercaseLetter && containsNumbers) return true;
	else return false;
}

exports.userValidation = (isPasswordMatch, isPasswordFormat, isPasswordLength) => {
	if(isPasswordMatch && isPasswordFormat && isPasswordLength) return true
	else return false
}