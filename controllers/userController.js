const User = require("../models/userModel");
const Product = require("../models/productModel");
const validation = require("../public/js/validation");
const _ = require("lodash");
const bcrypt = require("bcrypt");
const findOrCreate = require('mongoose-findorcreate')
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const passport = require("passport");
const auth = require("../public/js/auth");


passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});


passport.use(new GoogleStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: "http://localhost:4000/auth/google/main",
    userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo'
  },
  function(accessToken, refreshToken, profile, cb) {
  	User.findOrCreate({ googleId: profile.id }, 
    	{ 
    		email:profile.emails[0].value,
    		fullName: [
    			{
    				givenName: profile.name.givenName,
    				familyName: profile.name.familyName

    			}
    		]
    	},
    	function (err, user) {
			let token = {access: auth.createAccessToken(user)}
			token = token.access
			localStorage.setItem('token', JSON.stringify(token));

   	  	return cb(err, user);
    });
  }
));

exports.createNewUser = (user) => {
	const numberOfSalts = 10;
	const firstName = _.capitalize(user.firstName);
	const lastName = _.capitalize(user.lastName);
	const encryptedPass = bcrypt.hashSync(user.password, numberOfSalts)
	const newUser = new User({
		fullName:[
			{
				givenName: firstName,
				familyName: lastName 
			}
		],
		email: user.email,
		password: encryptedPass,
	})

	const isPasswordMatch = validation.checkMatchPassword(user.password, user.confirmPassword);
	const isPasswordFormat = validation.checkFormatPassword(user.password);
	const isPasswordLength = validation.checkPasswordLength(user.password);
	const isValidate = validation.userValidation(isPasswordMatch,isPasswordFormat,isPasswordLength);

	return User.find({email: user.email}).then((userFound, err) => {	
		if(userFound.length !== 0 || isValidate === false) return false;
		else{
			newUser.save();
			return true;
		}
	})
};

exports.loginUser = (user) => {
	return User.findOne({email: user.email}).then(emailFound =>{
		if(emailFound == null) return false;
		else{
			const isPasswordCorrect = bcrypt.compareSync( user.password,emailFound.password);
			if(isPasswordCorrect){
				const accessToken = {access: auth.createAccessToken(emailFound)};
				return accessToken;
			}
			else return false;
		}
	})
};

exports.getAllUsers = (userData) => {
	console.log(userData)
	let promises = [
		User.find({}).then(result => { return result}),
		User.count({$and:[{isAdmin: false},{isActive: true}]}).then(result => {return result}),
		Product.count({isActive: true}).then(product => { return product}),
		Product.find({}).then(product => {return product}),
		Product.aggregate([
			{$unwind: "$customer"},
			{$match: {"customer.isBought": true}},
			{$project: 
				{
					productName: "$productName",
					description: "$description",
					price: "$price",
					_id: "$customer._id",
					userId: "$customer.userId",
					isBought: "$customer.isBought"
				}
			}
		]).then( result => {
			return result
		}),
		Product.aggregate([
				
				{$match: {"customer.isBought": true}},
				{$count: "customer"}
		]).then( result => {
			return result
		})
	]
	return Promise.all(promises).then( resultPromise => { return resultPromise})
};

exports.setAdmin = (userDetails) => {
	const id = userDetails.admin;
	return User.findById(id).then(result => {
			if(result.isAdmin) result.isAdmin  = false;
			else result.isAdmin = true;
	   	result.save();	
	})
};

exports.fetch = (userData) => {
		let promises = [
			Product.find({isActive: true}).then(products => {return products }),
			User.find({_id:userData.id}).then(user => {return user}),
			Product.find({$and: [{"customer.userId":userData.id},{"customer.isBought":true}]}).then(products => {return products}),
			Product.count({$and: [{"customer.userId":userData.id},{"customer.isBought":true}]}).then(products => {return products})		
		]

		return Promise.all(promises).then(resultPromise => {return resultPromise});
		
};

exports.orderProduct = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.order.push({productId: data.orderId})

		return user.save().then((user, err) => {
			if(err) return false;
			else return true;
		})
	})

	let isOrderUpdated = await Product.findById(data.orderId).then(orders => {
		orders.customer.push({userId: data.userId});

		return orders.save().then((order, err) => {
			if(err) return false;
			else return true;
		})
	})

	if(isUserUpdated && isOrderUpdated) return true;
	else return false;
}

exports.removeProduct = (details) => {
	console.log(details)
	let productId = details.prodId;
	let userId = details.userId;
	userId = userId.substring(1,userId.length-1)
	return Product.findOneAndUpdate({$and: [{"customer.isBought":true},{"customer.userId":userId}]}, {"customer.$.isBought": false, "customer.$.userId": " "}, {multi: true}).then(products => {
		console.log(products)
		return products
	})

}

