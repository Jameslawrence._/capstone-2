const Product = require("../models/productModel");
const auth = require("../public/js/auth");
const _ = require("lodash");

exports.createProduct = (products) => {
	const prodName = _.capitalize(products.prodName);
	const description = _.capitalize(products.description);
	const price = products.price;

	const newProduct = new Product({
		productName: prodName,
		description: description,
		price: price
	})
	return Product.findOne({productName: prodName}).then(product => {
		if(product) return false;
		else{
			newProduct.save();
			return product;
		}
	})	
};

exports.archiveProduct = (product) => {
	const prodId = product.prodId;
	return Product.findById(prodId).then(product => {
		if(product.isActive) product.isActive = false;
		else product.isActive = true;
		product.save();
	})
};

exports.getProduct = (product) => {
	return Product.find({_id: product.id}).then(product => {
		return product;
	})
}

exports.updateProduct = (products, prodId) => {
	return Product.findById(prodId.id).then(product => {
		product.productName = products.prodName;
		product.description = products.description;
		product.price = products.price;
		product.save();
	})
}