//jshint esversion: 6
require("dotenv").config()
const express = require("express");
const mongoose = require("mongoose");
const ejs = require("ejs");
const passport = require('passport')

const app = express();
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// Middlewares
app.set("view engine", "ejs");
app.use(express.static('public'))
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(passport.initialize());
// Database Info
const connect = mongoose.connection;
mongoose.connect(process.env.URL, {useNewUrlParser: true, useUnifiedTopology: true});
connect.on("error", console.error.bind(console, "ERROR: Failed to connect to the Database"));
connect.once("open", () => { console.log("Successfully connected to the Database")});


app.use('/', userRoutes);
app.use('/', productRoutes);

app.listen(process.env.PORT || process.env.LOCAL_PORT, () => {
	console.log("Server is running")
})