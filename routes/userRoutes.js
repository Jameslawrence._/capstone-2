const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const passport = require("passport");
const auth = require("../public/js/auth");
const LocalStorage = require("node-localstorage").LocalStorage;

localStorage = new LocalStorage('./Token');


router.get("/", (req, res) => {
	res.render("index");
});

router.get("/signup", (req, res) => {
	res.render("signup");
});

router.get("/login", (req, res) => {
	localStorage.removeItem('token');
	res.render("login");
});

router.get("/user", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	userController.fetch(userData).then(resultFromController => {
		res.render("userPage", {
			allActiveProducts: resultFromController[0],
			user: resultFromController[1],
			userOrders: resultFromController[2],
			numberOfUserOrders: resultFromController[3]			
		})
	}) 
});


router.get("/main", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	userController.getAllUsers(userData).then(resultFromController => {
		console.log(resultFromController[4])			
		res.render("main",
			{ 
				Users: resultFromController[0],
				numberOfActiveUsers: resultFromController[1],
				numberOfActiveProducts: resultFromController[2],
				products: resultFromController[3],
				boughtProducts: resultFromController[4],
				numberOfBoughtProducts: resultFromController[5]

			}
		);
	}) 
});


router.post("/main/setAsAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	if(userData.isAdmin){
	userController.setAdmin(req.body).then(resultFromController => {
		res.redirect("/main");
	})}else res.redirect("/main");
});

router.get('/auth/google',
  passport.authenticate('google', { scope: ['profile','email'] })
 );

router.get("/auth/google/main", 
  passport.authenticate("google", { failureRedirect: "/login" }),
  function(req, res) {
  	const userData = auth.decode(localStorage.getItem('token'))
  	if(userData.isAdmin) res.redirect("/main");
  	else res.redirect("/user");    
});

// Signup Function 
router.post("/signup", (req, res) => {
	userController.createNewUser(req.body).then(resultFromController => {
		if(resultFromController) res.redirect("/login");
		else res.render("signup");
	})
});

// Login Function 
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => {
		if(resultFromController){
			if(resultFromController.isAdmin){
				localStorage.setItem('token', JSON.stringify(resultFromController.access));
				res.redirect("/main");				
			}else{
				localStorage.setItem('token', JSON.stringify(resultFromController.access));
				res.redirect("/user");
			}
		}else res.render("login")})
});


router.post('/order', (req, res) => {
	let id = req.body.userId;
	id = id.substring(1,id.length-1)
	let data = {
		userId: id,
		orderId: req.body.orderId
	}
	userController.orderProduct(data).then(resultFromController => {
		if(resultFromController) res.redirect("/user");
		else res.redirect("/login");
	})		
});

router.post("/product/removeProduct", auth.verify, (req, res) => {
	userController.removeProduct(req.body).then(resultFromController => {
		res.redirect("/user");
	})	
});

module.exports = router;