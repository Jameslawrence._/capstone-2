const express = require("express");
const router = express.Router();
const auth = require("../public/js/auth");
const productController = require("../controllers/productController");


router.post("/product-create", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	if(userData.isAdmin){	
	productController.createProduct(req.body).then(resultFromController => {
		if(resultFromController) res.redirect("/main");
		else res.redirect("/main");
	})}else res.redirect("/login");
});

router.post("/product/disableProduct", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	if(userData.isAdmin){	
	productController.archiveProduct(req.body).then(resultFromController => {
		res.redirect("/main");
	})}else res.redirect("/login");
});

router.get("/product/:id", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => {
		res.render("productPage", {Products: resultFromController});
	})
})

router.post("/product-update/:id", auth.verify, (req, res) => {
	const userData = auth.decode(localStorage.getItem('token'))
	if(userData.isAdmin){	
	productController.updateProduct(req.body, req.params).then(resultFromController => {
		res.redirect("/main");
	})}else res.redirect("login");

})


module.exports = router;