const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	productName: String,
	description: String,
	price: Number,
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},	
	customer: [
		{
			userId: String,
			orderOn: {
				type: Date,
				default: new Date()
			},
			isBought: {
				type: Boolean,
				default: true
			}
		}
	]
});



module.exports = mongoose.model("Product", productSchema);