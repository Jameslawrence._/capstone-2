const mongoose = require("mongoose");
const findOrCreate = require('mongoose-findorcreate')

const userSchema = new mongoose.Schema({
	fullName: [
		{
			givenName:
			{
				type: String,
				required: [true, "Given Name is required"]
			},
			familyName:
			{
				type: String,
				required: [true, "Family name is required"] 
			}
		}
	],
	email: String,
	googleId: String,
	password: String,
	isActive: {
		type: Boolean,
		default: true
	},
	isAdmin: {
		type: Boolean,
		default: false 
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	order: [
		{
			productId: String,
			totalAmount: Number,
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "pending"
			}
		}
	]
})

userSchema.plugin(findOrCreate);

module.exports = mongoose.model("User", userSchema);